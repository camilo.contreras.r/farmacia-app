### Tecnologías

- Express.js: Express.js, or simply Express, es un marco de aplicación web para Node.js
- Node.js: Node.js es un entorno de código abierto, multiplataforma, en tiempo de ejecución de JavaScript que ejecuta código JavaScript fuera de un navegador

- MOCHA es un framework de pruebas para Node JS que puede ejecutarse desde la consola o desde un navegador. Como su propia web indica, permite realizar pruebas asíncronas de manera sencilla y divertida. Al ejecutar los test, permite la presentación de informes flexibles y precisos.

- CHAI es una librería de aserciones BDD/TDD para Node JS y navegador, que puede ser armónicamente emparejado con cualquier framework Javascript.

- Chai HTTP es una extensión de la librería CHAI, que permite realizar pruebas de integración con llamadas HTTP utilizando las aserciones de CHAI y todos los métodos de HTTP: GET, POST, PUT, DELETE, PATCH…

# Proceso de instalación backend



**Descargar código fuente**

`git clone https://gitlab.com/camilo.contreras.r/farmacia_backend.git`

Ejecutar el siguiente comando npm:

    npm i && npm run app

Para testear uno de los servicios de ejemplo. Ejecutar el siguiente comando:

    npm run test


# Proceso de instalación front end

**Descargar código fuente**

`git clone https://gitlab.com/camilo.contreras.r/farmacia_frontend.git`

Ejecutar el siguiente comando npm:

    npm i && npm run start


