import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class RestApiService {
  
  // Define API backend cloud
  apiURL = 'https://immense-sea-90381.herokuapp.com';

  constructor(private http: HttpClient) { }


  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }  

  // HttpClient API get() method => Fetch regiones list
  getRegiones(): Observable<any> {
    return this.http.get<any>(this.apiURL + '/api/farmacia/getRegions')
    .pipe(
      retry(0),
      catchError(this.handleError)
    )
  }

  // HttpClient API get() method => Fetch comunas combo
  getComunasByRegion(id): Observable<any> {
    return this.http.get<any>(this.apiURL + '/api/farmacia/getComunasByRegion?regId=' + id)
    .pipe(
      retry(0),
      catchError(this.handleError)
    )
  }

  // HttpClient API get() method => Fetch comunas farmacias
  getLocalesRegion(id): Observable<any> {
    return this.http.get<any>(this.apiURL + '/api/farmacia/getLocalesRegion?regId=' + id)
    .pipe(
      retry(0),
      catchError(this.handleError)
    )
  }  

  // Error handling 
  handleError(error) {
     let errorMessage = '';
     if(error.error instanceof ErrorEvent) {
       // Get client-side error
       errorMessage = error.error.message;
     } else {
       // Get server-side error
       errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
     }
     window.alert(errorMessage);
     return throwError(errorMessage);
  }

}