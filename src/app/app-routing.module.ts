import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FarmaciaListComponent} from './farmacia-list/farmacia-list.component';
import { HttpClientModule } from '@angular/common/http';

const routes: Routes = [
  { path: '', component: FarmaciaListComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes),HttpClientModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
