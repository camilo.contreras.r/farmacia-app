import { Component, OnInit, ViewChild } from '@angular/core';
import { RestApiService } from '../shared/rest-api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-farmacia-list',
  templateUrl: './farmacia-list.component.html',
  styleUrls: ['./farmacia-list.component.css']
})
export class FarmaciaListComponent implements OnInit {

  listaRegiones: any = [];
  regionSeleccionada: number = 0;
  comunaSeleccionada: any;
  listaComunas: any;
  listaLocales: any = [];
  listaNombres: any[] = [];
  nLocalSeleccionado: string = null;
  showData: boolean = false;
  lat: any;
  long:any;
  listaLocalesFiltrada: any = [];

  constructor(public restApi: RestApiService, public router: Router) { }

  ngOnInit() {
    this.loadRegiones();
  }

  // Get regiones list
  loadRegiones() {
    return this.restApi.getRegiones().subscribe((data: {}) => {
      this.listaRegiones = data;
    })
  }

  //OnChangeRegion
  onChangeRegion(){
    /*this.restApi.getComunasByRegion(this.regionSeleccionada).subscribe((data: {}) => {
      console.log(data);
      this.listaComunas = data;
    });*/

    this.restApi.getLocalesRegion(this.regionSeleccionada).subscribe((data: {}) => {
      this.listaLocales = data;
      this.getNombresLocales(this.listaLocales);
    });
  }

  getNombresLocales(q: any){
    this.listaNombres = [];
    this.nLocalSeleccionado = null;
    var qcache = {};
    var nuevo = q.reverse().filter(e => {
      return e.local_nombre ? (qcache[e.local_nombre] ? false : qcache[e.local_nombre] = 1) : true;
    }).reverse();

    nuevo.forEach(e => {
      this.listaNombres.push(e.local_nombre);
    });
  }

  buscarLocal(){
    this.listaLocalesFiltrada = this.listaLocales.filter(e => (e.local_nombre === this.nLocalSeleccionado));
    this.lat = parseFloat(this.listaLocalesFiltrada[0].local_lat);
    this.long = parseFloat(this.listaLocalesFiltrada[0].local_lng);
    this.showData = true;
  }
}
